package com.liuyao;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class main_subs_0 {


public static RemoteObject  _appstart(RemoteObject _args) throws Exception{
try {
		Debug.PushSubsStack("AppStart (main) ","main",0,main.ba,main.mostCurrent,14);
if (RapidSub.canDelegate("appstart")) return main.remoteMe.runUserSub(false, "main","appstart", _args);
Debug.locals.put("Args", _args);
 BA.debugLineNum = 14;BA.debugLine="Sub AppStart (Args() As String)";
Debug.ShouldStop(8192);
 BA.debugLineNum = 20;BA.debugLine="ly.Initialize";
Debug.ShouldStop(524288);
main._ly.runClassMethod (com.liuyao.liuyao.class, "_initialize",main.ba);
 BA.debugLineNum = 21;BA.debugLine="test";
Debug.ShouldStop(1048576);
_test();
 BA.debugLineNum = 23;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			Debug.ErrorCaught(e);
			throw e;
		} 
finally {
			Debug.PopSubsStack();
		}}

private static boolean processGlobalsRun;
public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        main_subs_0._process_globals();
main.myClass = BA.getDeviceClass ("com.liuyao.main");
liuyao.myClass = BA.getDeviceClass ("com.liuyao.liuyao");
lypp.myClass = BA.getDeviceClass ("com.liuyao.lypp");
calendar.myClass = BA.getDeviceClass ("com.liuyao.calendar");
		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static RemoteObject  _process_globals() throws Exception{
 //BA.debugLineNum = 7;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 8;BA.debugLine="Private ly As liuyao";
main._ly = RemoteObject.createNew ("com.liuyao.liuyao");
 //BA.debugLineNum = 9;BA.debugLine="Private cal As sxwnl";
main._cal = RemoteObject.createNew ("com.sxwnljava.sxwnl");
 //BA.debugLineNum = 10;BA.debugLine="Private bc As ByteConverter";
main._bc = RemoteObject.createNew ("anywheresoftware.b4a.agraham.byteconverter.ByteConverter");
 //BA.debugLineNum = 11;BA.debugLine="Dim ser As Server";
main._ser = RemoteObject.createNew ("anywheresoftware.b4j.object.ServerWrapper");
 //BA.debugLineNum = 12;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _test() throws Exception{
try {
		Debug.PushSubsStack("test (main) ","main",0,main.ba,main.mostCurrent,24);
if (RapidSub.canDelegate("test")) return main.remoteMe.runUserSub(false, "main","test");
RemoteObject _dt = RemoteObject.createImmutable(0L);
 BA.debugLineNum = 24;BA.debugLine="Sub test";
Debug.ShouldStop(8388608);
 BA.debugLineNum = 27;BA.debugLine="Dim dt As Long=DateTime.Now";
Debug.ShouldStop(67108864);
_dt = main.__c.getField(false,"DateTime").runMethod(true,"getNow");Debug.locals.put("dt", _dt);Debug.locals.put("dt", _dt);
 BA.debugLineNum = 28;BA.debugLine="DateTime.DateFormat=\"yyyy-MM-dd hh-mm-ss\"";
Debug.ShouldStop(134217728);
main.__c.getField(false,"DateTime").runMethod(true,"setDateFormat",BA.ObjectToString("yyyy-MM-dd hh-mm-ss"));
 BA.debugLineNum = 29;BA.debugLine="Log(\"阳历:\"&DateTime.Date(dt))";
Debug.ShouldStop(268435456);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.concat(RemoteObject.createImmutable("阳历:"),main.__c.getField(false,"DateTime").runMethod(true,"Date",(Object)(_dt)))));
 BA.debugLineNum = 30;BA.debugLine="Log(\"农历:\"&cal.ad2lunar(dt))";
Debug.ShouldStop(536870912);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.concat(RemoteObject.createImmutable("农历:"),main._cal.runMethod(true,"ad2lunar",(Object)(_dt)))));
 BA.debugLineNum = 31;BA.debugLine="Log(\"干支:\"&cal.ad2ganzhi(dt,True))";
Debug.ShouldStop(1073741824);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.concat(RemoteObject.createImmutable("干支:"),main._cal.runMethod(true,"ad2ganzhi",(Object)(_dt),(Object)(main.__c.getField(true,"True"))))));
 BA.debugLineNum = 33;BA.debugLine="Log(ly.guamaqugua0(\"131\"))";
Debug.ShouldStop(1);
main.__c.runVoidMethod ("Log",(Object)(main._ly.runClassMethod (com.liuyao.liuyao.class, "_guamaqugua0",(Object)(RemoteObject.createImmutable("131")))));
 BA.debugLineNum = 41;BA.debugLine="End Sub";
Debug.ShouldStop(256);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			Debug.ErrorCaught(e);
			throw e;
		} 
finally {
			Debug.PopSubsStack();
		}}
}