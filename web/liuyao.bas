﻿Type=Class
Version=4.2
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private s60jz() As String
	Private sTiangan(),sWuxing(),sLiuqin(),sLiushen() As String
	Private sDizhi(),sGuaDanNames(),sGuaLyNames() As String
	Private mGuaming As Map
	Private sx As sxwnl
	Public sYinyao="▅▅　▅▅"&CRLF,sYangyao="▅▅▅▅▅"&CRLF As String
	Type gua_dan(y1 As Boolean,y2 As Boolean,y3 As Boolean,name As String)
	Type gua_ly(y1 As Boolean,y2 As Boolean,y3 As Boolean,y4 As Boolean,y5 As Boolean,y6 As Boolean,name As String)
	'定义六爻卦
	Type ly_sizhu(nian As String,yue As String,ri As String,shi As String,ke As String,fen As String)
	Type ly_yao(wx As Int,lq As String,ls As String,yy As Boolean)
	'六爻卦由(本卦)6个爻,变卦六个爻，动爻,伏爻,卦名，四柱，世应，卦宫组成
	Type ly_gua(yao_me(6) As ly_yao,yao_bg(6) As ly_yao,yao_dong() As ly_yao,yao_fu() As ly_yao,name As String,sizhu As ly_sizhu,me_s As Int,me_y As Int,bg_s As Int,bg_y As Int,guagong As Int)
	'六爻卦定义完毕
End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
	s60jz=Array As String("","甲子","乙丑","丙寅","丁卯","戊辰","己巳","庚午","辛未","壬申","癸酉","甲戌","乙亥","丙子","丁丑","戊寅","己卯","庚辰","辛巳","壬午","癸未","甲申","乙酉","丙戌","丁亥","戊子","己丑","庚寅","辛卯","壬辰","癸巳","甲午","乙未","丙申","丁酉","戊戌","己亥","庚子","辛丑","壬寅","癸卯","甲辰","乙巳","丙午","丁未","戊申","己酉","庚戌","辛亥","壬子","癸丑","甲寅","乙卯","丙辰","丁巳","戊午","己未","庚申","辛酉","壬戌","癸亥","癸亥")
	sTiangan=Array As String("","甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬" ,"癸")
	sDizhi=Array As String("","子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌","亥")
	sGuaDanNames=Array As String("","乾","兑","离","震","巽","坎","艮","坤")
	sGuaLyNames=Array As String()
	sWuxing=Array As String("土","水","火","木","金")
	mGuaming.Initialize
	'乾宫
	mGuaming.Put(11,"乾为天")
	mGuaming.Put(15,"天风姤")
	mGuaming.Put(17,"天山遁")
	mGuaming.Put(18,"天地否")
	mGuaming.Put(58,"风地观")
	mGuaming.Put(78,"山地剥")
	mGuaming.Put(38,"火地晋(游)")
	mGuaming.Put(31,"火天大有(归)")
	
End Sub
'根据卦码取卦,总卦
public Sub guamaqugua0(gm As String) As String
	Dim gmstr As String=gm
	If gmstr.Length>8 Then Return "卦码非法"
	Dim sSg,sXg,sBg As Int
	sSg=gmstr.SubString2(0,1)
	sXg=gmstr.SubString2(1,2)
	sBg=gmstr.SubString2(2,gmstr.Length)
	Dim ret As String
	ret=guamaqugua1(sSg)&guamaqugua1(sXg)
	'爻图像化
	ret=ret.Replace("0",sYinyao)
	ret=ret.Replace("1",sYangyao)
	Return ret
End Sub
'根据卦码取卦,单卦
public Sub guamaqugua1(gm As Int) As String
	Dim ret As String=DEC_to_BIN(8-gm)
	Do While ret.Length<3
		ret="0"&ret
	Loop
	Return ret
End Sub
 '用途：将十进制转化为二进制
' 输入：Dec(十进制数)
' 输入数据类型：Long
' 输出：DEC_to_BIN(二进制数)
' 输出数据类型：String
' 输入的最大数为2147483647,输出最大数为1111111111111111111111111111111(31个1)
Public Sub DEC_to_BIN(Dec As Int) As String
    Dim sdec = "" As String
    Do While Dec > 0
        sdec = (Dec Mod 2)& sdec
        Dec = Dec / 2
    Loop
	Return sdec
End Sub

' 用途：将二进制转化为十进制
' 输入：Bin(二进制数)
' 输入数据类型：String
' 输出：BIN_to_DEC(十进制数)
' 输出数据类型：Long
' 输入的最大数为1111111111111111111111111111111(31个1),输出最大数为2147483647
Public Sub BIN_to_DEC(Bin As String) As Int
    Dim ret As Int
    For i = 0 To Bin.Length-1
        ret = ret * 2 + Bin.SubString2( i, i+1)
    Next
	Return ret
End Sub
Public Sub guama2guaming(gm As Int) As String
	Dim name As String
	Select gm
	Case 11
		name="乾为天"
	Case 15
		name="天风姤"
	Case 17
		name="天山遁"
	Case 18
		name="天地否"
	Case 58
		name="风地观"
	Case 78
		name="山地剥"
	Case 38
		name="火地晋"
	Case Else
		name=""
	End Select
	Return name
End Sub
Private Sub GuaMaQuDiZhi(gm As Int)
	Dim r(6) As String
	Dim base As Int
	Select gm
	Case 11
		base=1
	Case 44
		base=1
	Case 
	End Select
	r=quliuyaodizhi2(base)
	Return r
End Sub
Private Sub quliuyaodizhi2(base As Int) As String()
	Dim r(6) As String
	Dim ptr As Int
	For i=0 To r.Length-1
		ptr=base+2*i
		ptr=ptr mod 12
		r(i)=sDizhi(ptr)
	Next
	Return r
End Sub
'idx范围0-59例0=甲子
public Sub getsGz(idx As Int) As String
	If idx>-1 And idx<60 Then
		Return s60jz(idx)
	Else
		Return "ErrIdx"
	End If
	
End Sub
'根据序号0-59获取干支字符例如0=甲子 此乃原始算法版本
'Public Sub getsGz(idx As Int) As String
'	Dim i,j As Int
'	Dim ret As String
'	i=0
'	j=0
'	For k=0 To 59
'		ret=sTiangan(i)&sDizhi(j)
'		If k=idx Then
'			Exit
'		Else
'			Log(ret)
'		End If
'		If i=9 Then
'			i=0
'		Else
'			i=i+1	
'		End If
'		If j=11 Then
'			j=0
'		Else
'			j=j+1
'		End If
'	Next
'	Return ret
'End Sub